# STATUS

## What Works:

- session login
- creating/listing/detail/deleting profiles
- creating/lising/deleting departments


## What Don't Work:

- profiles created using the API won't be able to login
- tests for creation and removal of departments fail for the lack of an admin user
- you have to create a admin user manually


## Wishlist:

- Docker
- Not Using Django Rest Framework or understangind it better
- Documentation
- Make the auth endpoint show up on the root of the API
- Rename the main app to api and keeping it's URLs separated from the rest 
