"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers

from main.models import Profile
from main.api import RegistrationAPIView, ProfileAPIViewSet, DepartmentAPIViewSet
from main.serializers import ProfileSerializer


router = routers.DefaultRouter()
router.register(r'profile', ProfileAPIViewSet, basename='profile')
router.register(r'department', DepartmentAPIViewSet, basename='department')

urlpatterns = [
    # user registration
    path('api/registration/', RegistrationAPIView.as_view(), name='registration'),
    # profiles list, detail, update and delete
    path('api/', include(router.urls)),
    # TODO: endpoints for:
    #   password change (takes old and new password)
    #   department create
    #   department removal
    path('admin/', admin.site.urls),
    # authentication
    path('auth/', include('rest_framework.urls')),
]
