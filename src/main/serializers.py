from rest_framework import serializers

from .models import Profile, Department


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ['id', 'full_name', 'department', 'email', 'password']
        extra_kwargs = {
            'password': {'write_only': True}
        }


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'
