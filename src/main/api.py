from rest_framework import generics, viewsets, status
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated

from .models import Profile, Department
from .serializers import ProfileSerializer, DepartmentSerializer
from .permissions import SameDepartmentProfileUpdate

class RegistrationAPIView(generics.CreateAPIView):
    permission_classes = [AllowAny]
    serializer_class = ProfileSerializer


class ProfileAPIViewSet(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.filter(is_active=True)
    permission_classes = [SameDepartmentProfileUpdate, IsAuthenticated]


class DepartmentAPIViewSet(viewsets.ModelViewSet):
    serializer_class = DepartmentSerializer
    queryset = Department.objects.filter(is_active=True)
    permission_classes = [IsAdminUser]
