from rest_framework import permissions


class SameDepartmentProfileUpdate(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.user == obj:
            return True
        if request.method != 'DELETE' and request.user.department == obj.department:
            return True
