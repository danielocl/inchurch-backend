from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Department, Profile


class Auth(APITestCase):

    def setUp(self):
        self.department = Department()
        self.department.save()

        self.profile = Profile.objects.create_user(
            email='one@example.com',
            password='one123',
            full_name='Thing one',
            department=self.department,
        )

        self.endpoints = [
            # (must require authentication, url, accepted methods)
            (
                False, reverse('registration'), ['POST']
            ),
            (
                True, reverse('profile-list'), ['GET'],
            ),
            (
                True, reverse('profile-detail', args=[0]), ['GET', 'PATCH', 'DELETE']
            ),
            (
                True, reverse('department-list'), ['POST', 'DELETE']
            )
        ]

    def test_required_authentication(self):
        for auth_required, endpoint, methods in self.endpoints:
            for method in methods:
                if methods == 'POST':
                    response = self.client.post(endpoint, {}, format='json')

                    if auth_required:
                        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
                    else:
                        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

                if method == 'GET':
                    response = self.client.get(endpoint, format='json')

                    if auth_required:
                        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
                    else:
                        self.assertEqual(response.status_code, status.HTTP_200_OK)

                if method == 'PATCH':
                    response = self.client.patch(endpoint, {}, format='json')

                    if auth_required:
                        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
                    else:
                        self.assertEqual(response.status_code, status.HTTP_200_OK)

                if method == 'DELETE':
                    response = self.client.delete(endpoint, format='json')

                    if auth_required:
                        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
                    else:
                        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_should_be_able_to_list_profiles_once_authenticated(self):
        url = reverse('profile-list')

        client = self.client
        loggedin = client.login(username='one@example.com', password='one123')

        self.assertTrue(loggedin)

        response = client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

