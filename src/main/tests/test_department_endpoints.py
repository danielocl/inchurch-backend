from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Department, Profile


class DepartmentEndpoint(APITestCase):

    def setUp(self):
        self.departments = [
            Department.objects.create(name="Dept 1"),
            Department.objects.create(name="Dept 2")
        ]

        self.profile = Profile.objects.create_user(
            email='one@example.com',
            password='one123',
            full_name='Thing one',
            department=self.departments[0],
        )

    def test_create(self):
        url = reverse('department-list')

        self.client.login(username='one@example.com', password='one123')
        response = self.client.post(url, {'name': 'Test dept'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)


    def test_list(self):
        url = reverse('department-list')

        self.client.login(username='one@example.com', password='one123')
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_delete(self):
        department = self.departments[1]

        url = reverse('department-detail', args=[department.pk])

        self.client.login(username='one@example.com', password='one123')
        response = self.client.delete(url, format='json')

        self.assertFalse(Department.objects.filter(is_active=True,pk=department.pk).exists())

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(None, response.data)
