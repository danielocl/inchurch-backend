from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Department, Profile


class ProfileEndpoint(APITestCase):

    def setUp(self):
        self.department = Department()
        self.department.save()

        self.profiles = [
            Profile.objects.create_user(
                email='one@example.com',
                password='one123',
                full_name='Thing one',
                department=self.department,
            ),
            Profile.objects.create_user(
                email='two@example.com',
                password='two123',
                full_name='Thing two',
                department=self.department,
            ),
        ]

    def test_list(self):
        url = reverse('profile-list')

        loggedin = self.client.login(username='one@example.com', password='one123')
        self.assertTrue(loggedin, 'could not log in')

        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get(self):
        profile = self.profiles[0]

        url = reverse('profile-detail', args=[profile.id])

        loggedin = self.client.login(username='one@example.com', password='one123')
        self.assertTrue(loggedin, 'could not log in')

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({'id': 1, 'full_name': 'Thing one', 'department': 1, 'email': 'one@example.com'}, response.data)

    def test_update(self):
        profile = self.profiles[1]

        url = reverse('profile-detail', args=[profile.id])

        data = {'full_name': 'Thing Two'}

        loggedin = self.client.login(username='one@example.com', password='one123')
        self.assertTrue(loggedin, 'could not log in')

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({'id': 2, 'full_name': 'Thing Two', 'department': 1, 'email': 'two@example.com'}, response.data)

    def test_delete(self):
        profile = self.profiles[0]

        url = reverse('profile-detail', args=[profile.id])

        loggedin = self.client.login(username='one@example.com', password='one123')
        self.assertTrue(loggedin, 'could not log in')

        response = self.client.delete(url, format='json')

        self.assertFalse(Profile.objects.filter(is_active=True, pk=profile.pk).exists())

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(None, response.data)
