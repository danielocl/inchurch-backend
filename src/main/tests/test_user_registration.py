from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Department, Profile


class UserRegistrationEndpoint(APITestCase):

    def setUp(self):
        self.department = Department()
        self.department.save()

    def test_good_registration(self):
        url = reverse('registration')
        data = {
            'email': 'foo@example.com',
            'password': '123',
            'full_name': 'Bob Tables',
            'department': self.department.id
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Profile.objects.count(), 1)
        self.assertEqual(response.data, {
            'id': 1,
            'email': 'foo@example.com',
            'full_name': 'Bob Tables',
            'department': self.department.id
        })
