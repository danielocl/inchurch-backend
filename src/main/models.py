from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class Department(models.Model):
    is_active = models.BooleanField(default=True)
    name = models.CharField(max_length=100)

    def delete(self):
        self.is_active=False
        self.save()


class ProfileManager(BaseUserManager):

    def create_user(self, email, full_name, password=None, department=None):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")
        if not full_name:
            raise ValueError("User must have a first name")

        user = self.model(
            email=self.normalize_email(email)
        )
        user.full_name = full_name
        user.set_password(password)  # change password to hash
        user.is_admin = False
        user.is_staff = False
        user.department = department
        user.save(using=self._db)
        return user


class Profile(AbstractBaseUser):

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name', 'password']

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    email = models.CharField(max_length=100, unique=True)
    full_name = models.CharField(max_length=255)
    department = models.ForeignKey(Department, related_name='members', on_delete=models.PROTECT)

    objects = ProfileManager()

    def delete(self):
        self.is_active=False
        self.save()

